$(document).ready(function() {

  /**
   * 
   * @return public object literal containing function objects
   */
  LayersHandler = (function() {


    function showTotal() {
      sublayers[0].setSQL("SELECT * FROM statuskart_med_resultater");
      sublayers[0].setCartoCSS("#statuskart_med_resultater[resultat='ja']{line-color: #000000;polygon-fill:#003762} #statuskart_med_resultater[resultat='nei']{line-color: #000000;polygon-fill:#940E19} #statuskart_med_resultater[resultat='']{line-color: #000000;polygon-fill:#ACACAC}");
      cartodb.vis.Vis.addInfowindow(map, sublayers[0], ["valgdeltakelse","prosent_ja", "prosent_nei", "prosent_blankt", "kommune"]);

      var ja_count = 0;
      var nei_count = 0;
      var no_results = 0;
      var total = 0;

      DataHandler.getData("andreasroeed", "SELECT resultat,valgdeltakelse,prosent_ja,prosent_nei,prosent_blankt FROM statuskart_med_resultater", function(data) {
        var has_results = 0;
        var mean_participation = 0;
        console.log(data);
        for (var x = 0; x < data.rows.length; x++) {

          mean_participation += data.rows[x].valgdeltakelse;
          if (data.rows[x].resultat == "ja") {
            has_results++;
            ja_count++;
          } else if (data.rows[x].resultat == "nei") {
            nei_count++;
            has_results++;
          } else if (data.rows[x].resultat == "") {
            no_results++;
          } else {
            continue;
          }
        }

        total = ja_count + nei_count + no_results;

        var percent_ja = ((ja_count * 100) / has_results)
        var percent_nei = ((nei_count * 100) / has_results)

        mean_participation = (mean_participation/ has_results);
        var data = [percent_ja,percent_nei,mean_participation];

        ChartHandler.showTotal(data);

      });
    };

    var arr = [];
    function findHighest(array,size){
      var tmp = 0.0;
      var counter = size;
      var to_delete = 0;
        console.log("size == "+size);
      if(counter <= 1){
        console.log("size - "+size);
        return true; 
      }
      for (let x = 0; x < array.length;x++){
        if(tmp < array[x].valgdeltakelse && array[x].valgdeltakelse != null){
          tmp = array[x].valgdeltakelse;
        }
      }
      

      findHighest(array,l);
      
    

    }

    /**
     * @param  {string} css - styling
     * @param  {string} bigger - The value to compare with smaller
     * @param  {string} smaller - The value to compare with bigger
     * @return {undefined} 
     */
    function showResults(css, bigger, smaller,result) {
      sublayers[0].setSQL("SELECT * FROM statuskart_med_resultater WHERE " + bigger + " > " + smaller + " OR resultat='"+result+"'");
      sublayers[0].setCartoCSS(css);
      var bigger = bigger;
      //TODO: show 10 best?
      DataHandler.getData("andreasroeed", "SELECT prosent_nei,prosent_ja,prosent_blankt,resultat,kommune,valgdeltakelse FROM statuskart_med_resultater WHERE (" + bigger + " > " + smaller + ") OR resultat='"+result+"'", function(data) {
        let topRated = [];
        var copy = data.rows;
        var top_five = findHighest(copy,5);

        var data = [data.rows[0], data.rows[1], data.rows[2],data.rows[3],data.rows[4]];
        ChartHandler.decidedChart(data, bigger);

      });
    };
    /**
     * NOT in USE
     * @param  {string} attribute - The attribute to check
     * @param  {string} input - The input value to compare with @param attribute
     * @return {undefined} 
     */
    function byCount(attribute, input) {
      sublayers[0].setSQL("SELECT * FROM statuskart_med_resultater WHERE " + attribute + " >= " + input + "");
      sublayers[0].setCartoCSS(cssyes);
    };
    /**
     * @param  {string} attribute
     * @param  {string} input
     * @return {[undefined]}
     */
    function byName(attribute, input) {

      var result = "";
      var color = "";

      var inputString = "'" + input + "'";
      var mapQuery = "SELECT * FROM statuskart_med_resultater WHERE " + attribute + " ILIKE " + inputString;

      DataHandler.getData("andreasroeed",mapQuery,function(data){
        mapQuery = "SELECT * FROM statuskart_med_resultater WHERE " + attribute + " ILIKE " + inputString;
        var css = data.rows[0].resultat == "ja" ? cssyess : cssno;

        sublayers[0].setSQL(mapQuery);
        sublayers[0].setCartoCSS(css);
      });


    };

    return {
      showTotal: showTotal,
      showResults: showResults,
      byCount: byCount,
      byName: byName
    }

  })();

});